const express = require("express")
const PORT = 3000
const path = require("path")
const fs = require("fs")

const app = express()

const PUBLIC_DIRECTORY = path.join(__dirname, "/assets")

function getFileHTML(fileNameHTML){
    const fileHTML = path.join(`${fileNameHTML}`)
    return fs.readFileSync(fileHTML, "utf-8")
}

app.use(express.static(PUBLIC_DIRECTORY))

app.get("/", (req, res) => {
    res.writeHead(200, {'Content-Type': 'text/html'})
    res.end(getFileHTML("index.html"))
})

app.listen(PORT, () => {
    console.log(`Server Running In Port : ${PORT}`)
})